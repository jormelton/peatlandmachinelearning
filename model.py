#!/space/hall3/sitestore/eccc/crd/ccrn/users/rjm001/miniconda/envs/peatlandMLenv/bin/python3.8
# #!/home/rjm001/.conda/envs/peatlandMLenv/bin/python3.8

# import warnings filter
from warnings import simplefilter
# ignore all future warnings
simplefilter(action='ignore', category=FutureWarning)

import sys
import os
import math
import csv 
import numpy as np
import pandas as pd
import xarray as xr
import matplotlib
matplotlib.use('pdf')
import matplotlib.pyplot as plt
from sklearn import ensemble, metrics
#from sklearn.svm import LinearSVR
from sklearn.feature_selection import SelectFromModel
from sklearn.linear_model import LassoCV, RidgeCV, LinearRegression
from timeit import default_timer as timer
from sklearn.pipeline import Pipeline
import concurrent.futures


import pickle
from sklearn.feature_selection import RFECV,RFE
from sklearn.model_selection import LeaveOneGroupOut
import eli5
from eli5.sklearn import PermutationImportance

from sklearn.model_selection import cross_val_score

# LightGBM:
import lightgbm as lgb

import cartopy.crs as ccrs
import cartopy.feature

# ------------------------

MAX_EVALS = 250

# Set the number of blocks to divide up the training data so that
# each block (i.e. longitude slice) contains approximately the same
# number of grid cells with valid data. If "blocks" is defined (below)
# then it is used and nblocks is ignored/not used.

#nblocks=13

# Blocks for dividing the training data.
# Format: [lonmin, lonmax, latmin, latmax, area_name]
# Values are non-inclusive, so use the first tile that is OUTSIDE
# of the valid data. 

# *** NB: Due to the way checks are done in the blocking routine,
#         all regions with data in the fractions file must be specified.
#         Also, overlapping regions will confuse the checks, so 
#         these are best merged into a single block
#         (e.g. block containing Scotland also contains a part of Ireland).

# blocks = [[-142.46, -52.15, 41.01, 84.53, "Canada"],
#           [57.9, 93.95, 55.6, 67.29, "West Siberia"],
#           [14.93, 24.74, -3.47, 3.18, "Congo"],
#           [94.63, 141.45, -11.45, 6.35, "Indonesia"],
#           [165.99, 178.97, -47.16, -33.97, "New Zealand"],
#           [143.42, 148.93, -44.07, -39.15, "Tasmania"],
#           [3.28, 7.64, 50.36, 53.85, "Netherlands"],
#           [28.72, 33.65, 58.69, 61.47, "St.Petersburg"],
#           [-77.78, -72.71, -7.06, -3.0, "Amazonia"],
#           #[-8.07, -0.41, 54.24, 61.25, "Scotland"],
#           #[-10.73, -5.58, 51.07, 55.79, "Ireland"]
#           [-10.73, -0.41, 51.07, 61.25, "Scotland+Ireland"],
#           [-18.2, 31.5, 15.5, 31.5, "Sahara"],
#           [111.5, 144.2, -18.2, -34.2, "Australia"]
#          ]

blocks = [[-180, -121.25, - 60, 90, "A"],
          [-121.25, -111.25, -60, 90, "B"],
          [-111.25, -101.25, - 60, 90, "C"],
          [-101.25, -87.5, -60, 90, "D"],
          [-87.5, -72.5, -60, 90, "E"],
          [-72.5, -38.25, -60, 90, "F"],
          [-38.25, 5.75, -60, 90, "G"],
          [5.75, 19.75, -60, 90, "H"],
          [19.75, 30.25, -60, 90, "I"],
          [30.25, 48.5, -60, 90, "J"],
          [48.5, 63.5, -60, 90, "K"],
          [63.5, 78.5, -60, 90, "L"],
          [78.5, 123.0, -60, 90, "M"],
          [123.0, 180.0, -60, 90, "N"]
          ]

# if drop_features, then feature_subset is used to DROP only those in the array specified for feature_subset. 
# if false then it is used to KEEP only those in the array.
# To run with ALL features, don't provide drop_features and feature_subset in the PeatlandRegressor call.
drop_features = False

# You can specify this array in the call to PeatlandRegressor

removeJul12 = ['EVI_mean', 'EVI_min', 'EVI_max', 'EVI_std',
               'EVI2_mean', 'EVI2_min', 'EVI2_max', 'EVI2_std',
               'NDVI_mean', 'NDVI_min', 'NDVI_max', 'NDVI_std', 
               'NIR_reflect_mean', 'NIR_reflect_min', 'NIR_reflect_max', 'NIR_reflect_std',
               'SWIR1_mean', 'SWIR1_min', 'SWIR1_max', 'SWIR1_std', 
               'SWIR2_mean', 'SWIR2_min', 'SWIR2_max', 'SWIR2_std', 
               'SWIR3_mean', 'SWIR3_min', 'SWIR3_max', 'SWIR3_std',
               'green_reflect_mean', 'green_reflect_min', 'green_reflect_max', 'green_reflect_std',
               'blue_reflect_mean', 'blue_reflect_min', 'blue_reflect_max', 'blue_reflect_std',
               # now remove the min/max/std of the new seasonal ones as I only want the mean.
               'EVI_min_DJF', 'EVI_min_MAM', 'EVI_min_JJA', 'EVI_min_SON', 
               'EVI_max_DJF', 'EVI_max_MAM', 'EVI_max_JJA', 'EVI_max_SON',
               'EVI_stdDev_DJF', 'EVI_stdDev_MAM', 'EVI_stdDev_JJA', 'EVI_stdDev_SON',
               'EVI2_min_DJF', 'EVI2_min_MAM', 'EVI2_min_JJA', 'EVI2_min_SON',
               'EVI2_max_DJF', 'EVI2_max_MAM', 'EVI2_max_JJA', 'EVI2_max_SON',
               'EVI2_stdDev_DJF', 'EVI2_stdDev_MAM', 'EVI2_stdDev_JJA', 'EVI2_stdDev_SON',
               'NDVI_min_DJF', 'NDVI_min_MAM', 'NDVI_min_JJA', 'NDVI_min_SON',
               'NDVI_max_DJF', 'NDVI_max_MAM', 'NDVI_max_JJA', 'NDVI_max_SON',
               'NDVI_stdDev_DJF', 'NDVI_stdDev_MAM', 'NDVI_stdDev_JJA', 'NDVI_stdDev_SON',
               'NIR_reflectance_min_DJF', 'NIR_reflectance_min_MAM', 'NIR_reflectance_min_JJA', 'NIR_reflectance_min_SON',
               'NIR_reflectance_max_DJF', 'NIR_reflectance_max_MAM', 'NIR_reflectance_max_JJA', 'NIR_reflectance_max_SON',
               'NIR_reflectance_stdDev_DJF', 'NIR_reflectance_stdDev_MAM', 'NIR_reflectance_stdDev_JJA', 'NIR_reflectance_stdDev_SON',
               'SWIR1_reflectance_min_DJF', 'SWIR1_reflectance_min_MAM', 'SWIR1_reflectance_min_JJA', 'SWIR1_reflectance_min_SON', 
               'SWIR1_reflectance_max_DJF', 'SWIR1_reflectance_max_MAM', 'SWIR1_reflectance_max_JJA', 'SWIR1_reflectance_max_SON',
               'SWIR1_reflectance_stdDev_DJF', 'SWIR1_reflectance_stdDev_MAM', 'SWIR1_reflectance_stdDev_JJA', 'SWIR1_reflectance_stdDev_SON',               
               'SWIR2_reflectance_min_DJF', 'SWIR2_reflectance_min_MAM', 'SWIR2_reflectance_min_JJA', 'SWIR2_reflectance_min_SON',
               'SWIR2_reflectance_max_DJF', 'SWIR2_reflectance_max_MAM', 'SWIR2_reflectance_max_JJA', 'SWIR2_reflectance_max_SON',
               'SWIR2_reflectance_stdDev_DJF', 'SWIR2_reflectance_stdDev_MAM', 'SWIR2_reflectance_stdDev_JJA', 'SWIR2_reflectance_stdDev_SON',
               'SWIR3_reflectance_min_DJF', 'SWIR3_reflectance_min_MAM', 'SWIR3_reflectance_min_JJA', 'SWIR3_reflectance_min_SON',
               'SWIR3_reflectance_max_DJF', 'SWIR3_reflectance_max_MAM', 'SWIR3_reflectance_max_JJA', 'SWIR3_reflectance_max_SON',
               'SWIR3_reflectance_stdDev_DJF', 'SWIR3_reflectance_stdDev_MAM', 'SWIR3_reflectance_stdDev_JJA', 'SWIR3_reflectance_stdDev_SON',
               'green_reflectance_min_DJF', 'green_reflectance_min_MAM', 'green_reflectance_min_JJA', 'green_reflectance_min_SON',
               'green_reflectance_max_DJF', 'green_reflectance_max_MAM', 'green_reflectance_max_JJA', 'green_reflectance_max_SON',
               'green_reflectance_stdDev_DJF', 'green_reflectance_stdDev_MAM', 'green_reflectance_stdDev_JJA', 'green_reflectance_stdDev_SON',
               'blue_reflectance_min_DJF', 'blue_reflectance_min_MAM', 'blue_reflectance_min_JJA', 'blue_reflectance_min_SON',
               'blue_reflectance_max_DJF', 'blue_reflectance_max_MAM', 'blue_reflectance_max_JJA', 'blue_reflectance_max_SON',
               'blue_reflectance_stdDev_DJF', 'blue_reflectance_stdDev_MAM', 'blue_reflectance_stdDev_JJA', 'blue_reflectance_stdDev_SON',
               'red_reflectance_min_DJF', 'red_reflectance_min_MAM', 'red_reflectance_min_JJA', 'red_reflectance_min_SON',
               'red_reflectance_max_DJF', 'red_reflectance_max_MAM', 'red_reflectance_max_JJA', 'red_reflectance_max_SON',
               'red_reflectance_stdDev_DJF', 'red_reflectance_stdDev_MAM', 'red_reflectance_stdDev_JJA', 'red_reflectance_stdDev_SON',
               # remove the surface soil characteristics
               'OLM_soil_organic_carbon1', 'OLM_soilWaterContent1','OLM_sand1','OLM_clay1', 'OLM_bulkdensity1'
               ]

postVIF_thres5_sep27 = ['srad_DJF', 'vap_MAM', 'slope', 'SWIR3_reflectance_mean_SON', 'OLM_Soil_BulkDensity_30cm', 'tcurv',
                        'ro_SON', 'soil_DJF', 'geom', 'ro_JJA', 'OLM_Soil_OrganicC_30cm', 'maxMODIS_NPP', 'ws_DJF', 'pdsi_SON', 'swe_DJF', 'Dormancy_2']

postVIF_thres5_Oct27 = ['srad_DJF', 'vap_MAM', 'slope', 'OLM_Soil_BulkDensity_30cm', 'SWIR3_reflectance_mean_SON', 'tcurv', 'geom', 'soil_DJF', 'ro_SON', 'OLM_Soil_OrganicC_30cm', 'rough-scale', 'EVI_Area_1', 'minMODIS_NPP', 'ws_DJF', 'dev-magnitude', 'convergence', 'EVI_Amplitude_2', 'dev-scale', 'dyy', 'aspect-cosine', 'dy', 'aspect', 'dx', 'pdsi_SON', 'swe_DJF', 'spi', 'Dormancy_1', 'Dormancy_2', 'dxy']

postVIF_RFECV_Oct27 = ['srad_DJF', 'vap_MAM', 'slope', 'OLM_Soil_BulkDensity_30cm', 'SWIR3_reflectance_mean_SON', 'tcurv', 'geom', 'soil_DJF', 'ro_SON', 'OLM_Soil_OrganicC_30cm', 'EVI_Area_1', 'minMODIS_NPP', 'ws_DJF', 'dev-magnitude', 'pdsi_SON', 'swe_DJF', 'Dormancy_2']

postVIFfirsttest_Sep29 = ['srad_DJF', 'vap_MAM', 'slope', 'EVI_Amplitude_2', 'OLM_Soil_BulkDensity_30cm', 'SWIR3_reflectance_mean_SON', 'tcurv', 'geom', 'EVI_Area_2', 'soil_DJF', 'ro_SON', 'OLM_Soil_OrganicC_30cm', 'rough-scale', 'EVI_Area_1', 'minMODIS_NPP', 'ws_DJF', 'dev-magnitude', 'convergence', 'aspect-cosine', 'northness', 'aspect-sine', 'dx', 'dev-scale', 'dyy', 'pdsi_SON', 'swe_DJF', 'spi', 'Dormancy_1', 'Senescence_2', 'dxy']

postVIF_RFECV_Sep29 = ['srad_DJF', 'vap_MAM', 'slope', 'OLM_Soil_BulkDensity_30cm', 'SWIR3_reflectance_mean_SON', 'tcurv', 'geom', 'soil_DJF', 'ro_SON', 'OLM_Soil_OrganicC_30cm', 'minMODIS_NPP', 'ws_DJF', 'dev-magnitude', 'pdsi_SON', 'swe_DJF', 'Senescence_2']

quicktestOct1 = ['slope', 'SWIR3_reflectance_mean_SON', 'rough-magnitude', 'aspect-sine', 'soil_DJF', 'aspect', 'soil_JJA', 'NDVI_mean_MAM', 'aet_MAM', 'PalsarmeanHH', 'srad_MAM', 'ws_JJA', 'srad_DJF', 'ro_SON', 'pdsi_MAM', 'ro_DJF', 'pdsi_SON', 'EVI_Amplitude_1', 'ws_DJF', 'maxMODIS_NPP', 'OLM_CLAY_30cm', 'ro_MAM',
                 'OLM_Soil_BulkDensity_30cm', 'minMODIS_NPP', 'EVI_Amplitude_2', 'red_reflect_max', 'tcurv', 'geom', 'EVI_Area_2', 'vap_JJA', 'ro_JJA', 'OLM_Soil_OrganicC_30cm', 'rough-scale', 'EVI_Area_1', 'dev-magnitude', 'convergence', 'dev-scale', 'aspect-cosine', 'dx', 'northness', 'dyy', 'swe_DJF', 'spi', 'Dormancy_2', 'Dormancy_1', 'dxy']

# Main function.

    # Automatic feature selection. Choices for doSelect are:
    # 1) 'Ridge': RidgeCV - not recommended!
    # 2) 'Lasso': LassoCV  - not recommended!
    # 3) 'RFE': RFE (Recursive Feature Elimination) - specify the number of features desired
    # 3) 'RFECV': RFECV (Recursive Feature Elimination with CV) - automatically selects number of features
    # 4) 'PI' : Permutation Importance (using eli5) 
    # 5) 'RFEPI': RFECV followed by PI
    # 6) 'VIF' : Variance Inflation Factor Filter.
    # 7) False:  No selection, just sets up the expected format.

# if drop_features = True, then feature_subset is used to DROP only those in the array specified for feature_subset. 
# if false then it is used to KEEP only those in the array.
# To run with ALL features, drop_features = True and feature_subset = None in the PeatlandRegressor call.

#trainfile='/space/hall3/sitestore/eccc/crd/ccrp/crp102/peatlands/peatlandMachineLearning/input/PEATLAND_training_All+glaciers+Rio_Negro+Beni.nc'
#trainfile='/space/hall3/sitestore/eccc/crd/ccrp/crp102/peatlands/peatlandMachineLearning/input/PEATLAND_training+glaciers+Rio_Negro+Beni-deserts.nc'
#trainfile = '/space/hall3/sitestore/eccc/crd/ccrp/crp102/peatlands/peatlandMachineLearning/input/PEATLAND_training_+glaciers+Rio_Negro+Beni+masked_deserts.nc'
trainfile = '/space/hall3/sitestore/eccc/crd/ccrp/crp102/peatlands/peatlandMachineLearning/input/Peatland_training.nc'
#trainfile = '/space/hall3/sitestore/eccc/crd/ccrp/crp102/peatlands/peatlandMachineLearning/input/Peatland_training+Beni.nc'
#trainfile = '/space/hall3/sitestore/eccc/crd/ccrp/crp102/peatlands/peatlandMachineLearning/input/Peatland_training+Chiquitano.nc'
#trainfile = '/space/hall3/sitestore/eccc/crd/ccrp/crp102/peatlands/peatlandMachineLearning/input/Peatland_training+Rio_Negro.nc'
#trainfile = '/space/hall3/sitestore/eccc/crd/ccrp/crp102/peatlands/peatlandMachineLearning/input/Peatland_training+Dry_Chaco.nc'
#trainfile = '/space/hall3/sitestore/eccc/crd/ccrp/crp102/peatlands/peatlandMachineLearning/input/Peatland_training+Rio_Negro+Beni.nc'
#trainfile = '/space/hall3/sitestore/eccc/crd/ccrp/crp102/peatlands/peatlandMachineLearning/input/Peatland_training+Rio_Negro+Dry_Chaco.nc'

#predfile='/space/hall3/sitestore/eccc/crd/ccrp/crp102/peatlands/peatlandMachineLearning/input/vars_Mar23_2021_geom11_5am.nc'
predfile = '/space/hall3/sitestore/eccc/crd/ccrp/crp102/peatlands/peatlandMachineLearning/input/Predictors_Jul12_2021.nc'


inparams = {'boosting_type':'dart', 
          'class_weight':None,
            'colsample_bytree': 0.8251441062858274,
          'importance_type':'gain',
            'learning_rate': 0.18817013045111064,
           'max_bin':95, 
           'max_depth':6,
            'min_child_samples':10, 
            'min_child_weight':0.001, 
            'min_split_gain':0.0,
            'n_estimators':250, 
            'num_leaves':30, 
            'objective':None,
            'random_state':None, 
            'reg_alpha': 0.705705986914311,
            'reg_lambda': 0.9086692536858783,
            'subsample': 0.7341745532372549,
            'subsample_for_bin': 80000,
            'subsample_freq':0}    

# Set VIF threshold (only used for VIF feature selection).
vif_threshold=5

def main():
    PeatlandRegressor(fractions_file=trainfile, predictors_file=predfile, outputdir='output/vif_oct27_thresh5_vif_rfecv-noEcoregions-opt1-test/', outputfile="lgb.nc",
                      CV=True, model="lgb", feature_subset=postVIF_RFECV_Oct27, drop_features=False, selectFeatures=False, optimizeParams=False, params=inparams_Oct28)

class PeatlandRegressor():

  # Initialization procedure for the data model.
  def __init__(self, fractions_file, predictors_file, outputdir='output/', outputfile="output.nc",
               CV=True, model='lgb', feature_subset=None,drop_features=False, selectFeatures='RFE', optimizeParams=True, params={}):

    # make the output directory if it doesn't already exist.
    if not os.path.exists(outputdir):
      os.makedirs(outputdir)    
    if not os.path.exists(outputdir + "plots"): 
      os.makedirs(outputdir + "plots")
  
    print("Preparing data for run:",outputdir)
    
    # Open the fractions (training data) file
    self.fractions_xr = xr.open_dataset(fractions_file)
    self.lat = self.fractions_xr['lat'].values
    self.lon = self.fractions_xr['lon'].values
    fractions = self.fractions_xr.to_dataframe()
    fractions.reset_index(inplace=True)

    # Open the predictors file, excluding any variables that we don't want. Also get the number of grid cells.
    if (drop_features):
      pred = xr.open_dataset(predictors_file,drop_variables=feature_subset)
    else:
      pred = xr.open_dataset(predictors_file)[feature_subset]

    self.predictors = pred.to_dataframe()
    self.predictors.reset_index(inplace=True)

    # Save a copy of index -> coordinate mappings as a DataFrame
    self.coordinates = self.predictors[["lat", "lon"]]

    # Merge the fractions and predictors, yielding the training data
    self.training_data = fractions
    self.training_data.dropna(inplace=True) # Here we remove any nan in the training data.
    self.training_data = pd.merge(fractions, self.predictors, on=['lat', 'lon'])

    # Drop all NaN entries from the training data and predictors
    self.predictors.dropna(inplace=True)
    self.training_data.dropna(inplace=True)

    # Find the CV blocks for LOGO if doing optimization, selecting features or CV.
    if optimizeParams or CV or selectFeatures:  
       self.__blockData(outputdir)
    #sys.exit()
    # Automatic feature selection. Choices for doSelect are:
    # 1) 'Ridge': RidgeCV - not recommended!
    # 2) 'Lasso': LassoCV  - not recommended!
    # 3) 'RFE': RFE (Recursive Feature Elimination) - specify the number of features desired
    # 3) 'RFECV': RFECV (Recursive Feature Elimination with CV) - automatically selects number of features
    # 4) 'PI' : Permutation Importance (using eli5) 
    # 5) 'RFEPI': RFECV followed by PI
    # 6) 'VIF' : Variance Inflation Factor Filter.
    # 7) False:  No selection, just sets up the expected format.
    self.selectFeatures(outputdir,doSelect=selectFeatures)

    self.params = params

    if optimizeParams:
       print('Starting Bayesian hyperparamter optimization...')
       self.optimizeHyperParams(outputdir)
       
    # Initialization of regressor models (lrg, etr, xgb, etc.)
    self.initializeModels(optimizeParams)

    # Cross validation using the blocks defined at top of file.
    if CV:
      # Performance metrics
      self.crossValidation(outputdir, selected_model=model)
      self.generatePerformanceMetrics(outputdir)

    # Global prediction
    self.predictGlobalPeatlandFractions(model,outputdir)
    self.outputNetCDF(outputdir + outputfile)
    self.drawGlobalMap(model,outputdir)
   
 # ------------------------------------------------------------------------------------------------
  # Feature selection.

  def selectFeatures(self,outputdir,doSelect=True):
 
    print("Selecting features...")

    if doSelect != False:

      # Save a list of features from the training set
      features = self.training_data.columns[3:].tolist()
      
      # Chop off lon/lat, and save features to X, expected output to y
      X = self.training_data[features].values
      y = self.training_data['PEATLAND_P'].values.squeeze()

      # Define CV splitter LOGO and use with block/group labels.
      logo = LeaveOneGroupOut()

      if doSelect=='Ridge':
        estimator = RidgeCV(cv=list(logo.split(X, y, self.groups)))
        # Initialize SelectFromModel object, fit it to our data
        selector = SelectFromModel(estimator, threshold="mean").fit(X, y)
        supp = selector.get_support()

      if doSelect=='Lasso':
        estimator = LassoCV(max_iter=50000, cv=list(logo.split(X, y, self.groups)) ,n_jobs=-1, selection='cyclic')
        selector = SelectFromModel(estimator, threshold="mean").fit(X, y)
        supp = selector.get_support()
        
      if doSelect=='RFE':
        # Recursive Feature Elimination (RFE).
        estimator = lgb.LGBMRegressor(importance_type='gain') # using default params/gain        
        selector = RFE(estimator, n_features_to_select=23)
        selector.fit(X, y)
        
        # This below allows inspection of different feature numbers. But it is very 
        # computataionally expensive as RFE doesn't parallelize like RFECV. There is 
        # a brute force attempt here, but it didn't seem to work. Hence this is all commented out.
        
        # get a list of models to evaluate (inspiration from https://machinelearningmastery.com/rfe-feature-selection-in-python/) --This is very computationally expensive!
        # def get_models():
        #   models = dict()
        #   for i in range(10, 12):
        #     rfe = RFE(estimator=estimator, n_features_to_select=i)
        #     model = estimator
        #     models[str(i)] = Pipeline(steps=[('s',rfe),('m',model)],verbose=True)
        #   return models
        # 
        # # evaluate a give model using cross-validation
        # def evaluate_model(model, X, y):
        #   cv = logo.split(X, y, self.groups)
        #   scores = cross_val_score(model, X, y, scoring='neg_mean_squared_error', cv=cv, n_jobs=-1, error_score='raise')
        #   return scores
        # 
        # # get the models to evaluate
        # def get_models_parallel(i):
        #   rfe = RFE(estimator=estimator, n_features_to_select=i)
        #   return rfe    
        # 
        # start = timer()      
        # model = estimator
        # models = dict()
        # iz = np.arange(10,31)
        # with concurrent.futures.ProcessPoolExecutor() as executor:
        #   for i, rfe in zip(iz, executor.map(get_models_parallel, iz)):
        #     models[str(i)] = Pipeline(steps=[('s',rfe),('m',model)],verbose=True)          
        # #models = get_models()
        # train_time = timer() - start
        # print('time for get_models ',train_time)
        # # evaluate the models and store results
        # results, names = list(), list()
        # start = timer()
        # for name, model in models.items():
        #   scores = evaluate_model(model, X, y)
        #   results.append(scores)
        #   names.append(name)
        #   print('>%s %.3f (%.3f)' % (name, np.mean(scores), np.std(scores)))
        # train_time = timer() - start
        # print('time for evaluate_model ',train_time)
        # 
        # # Plot number of features vs cross-validation scores.
        # fig = plt.figure(figsize=[16,10])
        # plt.xlabel("Number of features selected")
        # plt.ylabel("Cross validation score")
        # plt.boxplot(results, labels=names, showmeans=True)
        # plt.savefig(outputdir + "rfe_scores.png")
        supp = selector.support_
        print("Optimal number of features : %d" % selector.n_features_)
        print(selector.ranking_)
        print("-------------")

      if doSelect=='RFECV' or doSelect=='RFEPI':
        # Recursive Feature Elimination with Cross Validation (RFECV).
        estimator = lgb.LGBMRegressor(importance_type='gain') # using default params/gain
        # Doesn't appear to use "metrics" parameter from estimator. And r2 scoring gives same results 
        # as scoring=None. Although explained_variance gives different scores, the same features are selected.
        # The neg_mean_squared_error is actually RMSE but since higher return scores are better than lower, it is 
        # made negative.
        # Increasing the number of steps from 1 can be useful to eliminate the worst features early on and 
        # will also speed up feature elimination.
        selector = RFECV(estimator, cv=logo.split(X, y, self.groups), scoring='neg_mean_squared_error', step=1, n_jobs=-1)
        #selector = RFECV(estimator, cv=logo.split(X, y, self.groups), scoring='max_error', step=1, n_jobs=-1)
        selector.fit(X, y)
        supp=selector.support_
        print("Optimal number of features : %d" % selector.n_features_)
        print(selector.ranking_)
        print("-------------")
        print(selector.grid_scores_)

      # summarize all features
        for i in range(X.shape[1]):
          print('Column: %d, Selected %s, Rank: %.3f' % (i, selector.support_[i], selector.ranking_[i]))
        print("++-------------")
        # Plot number of features vs cross-validation scores.
        fig = plt.figure(figsize=[16,10])
        plt.xlabel("Number of features selected")
        plt.ylabel("Cross validation score")
        plt.plot(range(1, len(selector.grid_scores_) + 1), selector.grid_scores_)
        plt.savefig(outputdir + "rfecv_scores.png")

        if doSelect=='RFEPI':
          # Filter out unused variables from our data sets
          bad_vars = [i for (i, v) in zip(features, supp) if not v]
          self.training_data.drop(bad_vars, axis=1, inplace=True)
          self.predictors.drop(bad_vars, axis=1, inplace=True)
          print('BAD vars determined by RFECV:',bad_vars)
          print ('-----------')
          # Retain the list of good features for future use
          features = [i for (i, v) in zip(features, supp) if v]
          print(features)

      if doSelect=='PI' or doSelect=='RFEPI':
        # Permutation Importance.
        estimator = lgb.LGBMRegressor(importance_type='gain') # using default params/gain

        if doSelect=='RFEPI':
          # Apply feature selection from previous step.
          X = self.training_data[features].values

        perm = PermutationImportance(estimator, cv=list(logo.split(X, y, self.groups)))
        selector = SelectFromModel(perm,threshold='median').fit(X, y)
        supp = selector.get_support()

        # Output weights.
        expl = eli5.explain_weights(selector.estimator_, feature_names=features, top=None)        
        print(eli5.format_as_text(expl))
        text_file = open(outputdir + "eli5_importance.txt", "w")
        text_file.write(eli5.format_as_text(expl))        
        text_file.close()

      if doSelect=='VIF':
        # Variance Inflation Factor Filter.

        # Fit model to get feature importances sorted from least important to most.
        estimator=lgb.LGBMRegressor(importance_type='gain')
        estimator.fit(X,y)
        feature_importance=estimator.booster_.feature_importance(importance_type='gain')
        sorted_idx=np.argsort(feature_importance)
        importance=np.vstack([np.asanyarray(features)[sorted_idx], feature_importance[sorted_idx]]).T

        # Compute importances as a percentage and extract sorted list of features.
        df_gain = pd.DataFrame({'Features': importance[:, 0], 'gain': importance[:, 1].astype('float')})
        df_gain['gain']=100*df_gain['gain']/(df_gain['gain'].sum())
        features=df_gain['Features'].to_list()

        # Set index to features, to prepare for mapping of gain to other dataframe.
        df_gain.set_index('Features',inplace=True)

        # Initialize dataframes.
        df=self.training_data[features]
        df_vif=pd.DataFrame(np.diag(np.linalg.inv(np.array(df.corr()))), index=df.columns, columns=['VIF']).sort_values(by='VIF',ascending=False)
        df_vif['gain(%)'] = df_vif.index.map(df_gain['gain'])

        df_drop = pd.DataFrame(columns=['Features', 'VIF', 'gain(%)'])
        
        # Loop until all VIFs are below specified threshold.
        i=1

        while (df_vif.VIF>vif_threshold).any() == True:
          print('Iteration:',i)
          print(df_vif)
          print()
          print('df_drop:')
          print (df_drop)
        
          # Extract the list of highly correlated features (i.e. with VIF>vif_threshold).
          correlated_features=df_vif.index[df_vif.VIF>vif_threshold]
          print('corr feat:')
          print(correlated_features)
          print()
          print('features in features ', features)
          # Loop over selected features. Drop the least important one if it is highly correlated with others.
          for feature in features:
            if feature in correlated_features:
              print('feature removed ',feature)
              df_drop = df_drop.append({'Features': feature, 'VIF': df_vif.loc[feature].VIF,
                                        'gain(%)': df_gain['gain'][df_gain.index==feature].values[0]}, ignore_index=True)
              df.drop(columns=feature, inplace=True)
              break
          print('added drops:', df_drop)
          # # Recompute VIF.
          # df_vif=pd.DataFrame(np.diag(np.linalg.inv(np.array(df.corr()))), index=df.columns, columns=['VIF']).sort_values(by='VIF',ascending=False)

          # Recompute feature importances. 

          # If this section is commented out, the importances used will be the same for each iteration (i.e. much faster).
       
          # features = df.columns[3:].tolist()   
          # print('features after drop:',features)
          # X = df[features].values
          # estimator.fit(X,y)

          # If this section is commented out, the importances used will be the same for each iteration.
          estimator.fit(df.values,y)

          feature_importance=estimator.booster_.feature_importance(importance_type='gain')
          sorted_idx=np.argsort(feature_importance)
          importance=np.vstack([np.asanyarray(df.columns)[sorted_idx], feature_importance[sorted_idx]]).T
          df_gain = pd.DataFrame({'Features': importance[:, 0], 'gain': importance[:, 1].astype('float')})
          print('df_gain', df_gain)
          df_gain['gain']=100*df_gain['gain']/(df_gain['gain'].sum())
          features=df_gain['Features'].to_list()
          df_gain.set_index('Features',inplace=True)

          # Recompute VIF.
          df_vif=pd.DataFrame(np.diag(np.linalg.inv(np.array(df.corr()))), index=df.columns, columns=['VIF']).sort_values(by='VIF',ascending=False)

          df_vif['gain(%)'] = df_vif.index.map(df_gain['gain'])
          i=i+1

        print()
        print('Retained Features after',i-1,'iterations')
        print()
        print(df_vif)
        print()
        print('Dropped features:')
        print(df_drop)
        print()
        features=df_vif.index.to_list()
        print('List of retained features:',len(features))
        print(features)
        self.features=features
        return 

      # Filter out unused variables from our data sets
      bad_vars = [i for (i, v) in zip(features, supp) if not v]
      self.training_data.drop(bad_vars, axis=1, inplace=True)
      self.predictors.drop(bad_vars, axis=1, inplace=True)

      # Retain the list of good features for future use
      features = [i for (i, v) in zip(features, supp) if v]
      self.features=features

    else:
      self.features = self.training_data.columns[3:].tolist()

    print(self.features)
    return

# ------------------------------------------------------------------------------------------------
  # Initialize all ML algorithms that will be tested
  def initializeModels(self,optimizeParams=False):
    
    print("Initializing model")
    self.models = {}

    # Linear Regression
    #self.models['lrg'] = LinearRegression()

    # Extra Trees Regressor
    #etr_parameters = {'n_estimators':500, 'random_state':0, 'n_jobs':-1}
    #self.models['etr'] = ensemble.ExtraTreesRegressor(**etr_parameters)

    # SciKit-learn Gradient Boosted Trees
    # This, as implimented, resulted in negative predictions.
    #gbr_parameters = {'n_estimators':500, 'random_state':0}
    #self.models['gbr'] = ensemble.GradientBoostingRegressor(**gbr_parameters)

    # SciKit-learn Histogram-based Gradient Boosting Regression Tree (based on LightGBM)
    #hgbr_parameters = {}
    #self.models['hgbr'] = ensemble.HistGradientBoostingRegressor()#**hgbr_parameters)

    # XGBoost - not yet implemented
    #D_train = xgb.DMatrix(self.training_data, label=Y_train)
    #D_test = xgb.DMatrix(self.predictors, label=Y_test)
    #xgb_parameters = {'n_estimators':500, 'random_state':0}
    #self.models['xgb'] = ensemble.GradientBoostingRegressor(**xgb_parameters)

    # LightGBM
    lgb_parameters = self.params
    #print ('lgb params: ', lgb_parameters)
    self.models['lgb'] = lgb.LGBMRegressor(**lgb_parameters)
    print(self.models['lgb'])

# ------------------------------------------------------------------------------------------------
  # Split (block) data, begin supervised learning and evaluation
  def crossValidation(self, outputdir, selected_model='lgb'):
          
    print("Performing cross validation...")

    # Initialize dataframe to hold CV result for each block.
    pred_cv=self.training_data[["lat", "lon"]].copy()
    pred_cv["PEATLAND_P"]=np.nan
    self.CVresults = []
    i=0

    # Begin blocked holdout cross validation with various models

    logo = LeaveOneGroupOut()

    # Chop off lon/lat, and save features to X, expected output to y
    X = self.training_data[self.features].values
    y = self.training_data['PEATLAND_P'].values.squeeze()

    for train_index, test_index in logo.split(X, y, self.groups):
      X_train, X_test = X[train_index], X[test_index]
      y_train, y_test = y[train_index], y[test_index]

      # Record the CV result from each classifier, for every block
      results_container = {}
      area_name=blocks[i][4]
      i=i+1
      results_container['area_name'] = area_name
      results_container['actual_values'] = y_test

      # Iterate through each ML model and fit it
      for model in self.models.keys():
        start = timer()
        self.models[model].fit(X_train, y_train)
        train_time = timer() - start
        print(model,area_name,'The baseline training time is {:.4f} seconds'.format(train_time))

        pred = self.models[model].predict(X_test)

        # Restrict values to between 0 and 100.
        pred = np.where(pred < 0, 0, pred)
        pred = np.where(pred > 100, 100, pred)

        results_container[model] = pred

      # Add the results of CV on this region to the list
      self.CVresults.append(results_container)

      # Update dataframe containing CV results for each block.
      df = pd.DataFrame(data=results_container[selected_model], dtype=float, columns=["PEATLAND_P"], index=self.training_data.index[test_index])
      pred_cv.update(df)

      # Output the selected features and their relative importance per block
      # this assumes LGB is the model
      feature_importance=self.models[model].booster_.feature_importance(importance_type='gain')
      sorted_idx=np.argsort(feature_importance)
      importance_array=np.vstack([np.asanyarray(self.features)[sorted_idx], feature_importance[sorted_idx]]).T
      np.savetxt(outputdir+'cv_feature_importance_gain_'+area_name.replace(' ','_')+'.csv',importance_array,delimiter=',',fmt="%s")

      #feature_importance=self.models[model].feature_importances_
      feature_importance=self.models[model].booster_.feature_importance(importance_type='split')
      sorted_idx=np.argsort(feature_importance)
      importance_array=np.vstack([np.asanyarray(self.features)[sorted_idx], feature_importance[sorted_idx]]).T
      np.savetxt(outputdir+'cv_feature_importance_split_'+area_name.replace(' ','_')+'.csv',importance_array,delimiter=',',fmt="%s")

    # Need to combine best selected features of each block into a single set to replace self.features.

    # Output CV results for all blocks.
    self.pd_glob=pred_cv
    self.outputNetCDF(outputdir+selected_model+"_cv.nc")

# ------------------------------------------------------------------------------------------------
  # Use Bayesian inference to determine the optimal hyperparamters for LightGBM
  def optimizeHyperParams(self,outputdir):
            
    from hyperopt import hp
    from hyperopt.pyll.stochastic import sample
    from hyperopt import tpe
    from hyperopt import Trials
    from hyperopt import fmin
    
    # Citation for hyperopt:
    #Bergstra, J., Yamins, D., Cox, D. D. (2013) Making a Science of Model Search: Hyperparameter Optimization in Hundreds of Dimensions for Vision Architectures. To appear in Proc. of the 30th International Conference on Machine Learning (ICML 2013).
      
    # Hyperparameter search space
    space = {
        'boosting_type': hp.choice('boosting_type', [{'boosting_type': 'gbdt', 'subsample': hp.uniform('gdbt_subsample', 0.5, 1)}, 
                                                     {'boosting_type': 'dart', 'subsample': hp.uniform('dart_subsample', 0.5, 1)},
                                                    # {'boosting_type': 'dart', 'num_boost_round':150}, #dart can't use early_stopping rounds so set to lower number.
                                                     {'boosting_type': 'goss', 'subsample': 1.0}]),
        'num_leaves': hp.quniform('num_leaves', 10, 50, 1), # default is 31
        'n_estimators': hp.quniform('n_estimators', 50, 300, 5), # aka num_iterations, num_boost_round, default is 100.
        'learning_rate': hp.uniform('learning_rate', 0.005, 0.4),
        'max_bin': hp.quniform('max_bin',25, 300, 5),
        'max_depth': hp.quniform('max_depth',-1, 15, 1),
        #'max_depth': hp.loguniform('max_depth', np.log(0.036), np.log(3000000))
        'subsample_for_bin': hp.quniform('subsample_for_bin', 20000, 300000, 20000),
        'min_child_samples': hp.quniform('min_child_samples', 5, 60, 5), # aka min_data_in_leaf, default is 20.
        #'min_sum_hessian_in_leaf' - other option for over-fitting. Default is 1E-3.
        'reg_alpha': hp.uniform('reg_alpha', 0.0, 1.0),
        'reg_lambda': hp.uniform('reg_lambda', 0.0, 1.0),
        #'extra_trees' boolean option for overfitting
        # 'path_smooth' : default is 0, can be >0.
        'colsample_bytree': hp.uniform('colsample_by_tree', 0.5, 1.0) # aka feature_ifraction, default is 1.
    }

    # Choose optimization algorithm (Tree Parzen Estimator)
    tpe_algorithm = tpe.suggest

    # Make a lgb dataset of the training data 
    X_train = self.training_data[self.features].values
    y_train = self.training_data['PEATLAND_P'].values.squeeze()
    train_set = lgb.Dataset(X_train, y_train)

    # Create file to save first results
    out_file = outputdir + 'gbm_trials.csv'
    of_connection = open(out_file, 'a')
    writer = csv.writer(of_connection)

    # Write the headers to the file
    #writer.writerow(['loss', 'params', 'iteration', 'estimators', 'train_time'])
    writer.writerow(['loss', 'params', 'iteration', 'train_time'])
    of_connection.close()

    # Keep track of results in trials object.

    # Global variable
    global ITERATION

    try:  # Try to load an already saved trials object, and increase the max evals.
      bayes_trials = pickle.load(open(outputdir+ "/gbm_trials.pkl", "rb"))
      ITERATION = len(bayes_trials.trials)
      max_trials = ITERATION + MAX_EVALS
      print("Rerunning from {} trials to {} (+{}) trials".format(ITERATION, max_trials, MAX_EVALS))
    except:  # Create a new trials object.
      bayes_trials = Trials()
      ITERATION = 0
      max_trials=MAX_EVALS
    print("Running {} trials".format(max_trials))

    groups=self.groups

# ------------------------------------------------------------------------------------------------
    # Define the objective function:
    def objective(params):

      """Objective function for Gradient Boosting Machine Hyperparameter Optimization"""

      from hyperopt import STATUS_OK
      
      # Keep track of evals
      global ITERATION
      ITERATION += 1

      # Retrieve the subsample if present otherwise set to 1.0
      subsample = params['boosting_type'].get('subsample', 1.0)

      # Extract the boosting type
      params['boosting_type'] = params['boosting_type']['boosting_type']
      params['subsample'] = subsample
      params['importance_type'] = 'gain'

      # Make sure parameters that need to be integers are integers
      for parameter_name in ['max_bin','max_depth','num_leaves', 'subsample_for_bin', 'min_child_samples', 'n_estimators']:
      #for parameter_name in ['num_leaves', 'subsample_for_bin', 'min_child_samples', 'n_estimators', 'max_bin']:
          params[parameter_name] = int(params[parameter_name])

      #if params['boosting_type'] == 'rf':
      #  params.update({'bagging_freq': int(params['bagging_freq'])})

      # Define CV splitter LOGO using the block/group labels.
      logo = LeaveOneGroupOut()

      start = timer()

      # Perform cross validation
      '''
      cv_results = lgb.cv(params, train_set,  
                          folds=logo.split(X_train, y_train, groups),
                          #early_stopping_rounds = 15, 
                          #metrics = ['huber','quantile'], seed = 50,
                          metrics = ['rmse'], #seed = 50,
                          stratified=False,shuffle=False,
                          verbose_eval=50)
      '''

      cv_scores = cross_val_score(lgb.LGBMRegressor(**params), X_train, y_train,
                                  cv=logo.split(X_train, y_train, groups),
                                  #scoring='max_error', n_jobs=-1)
                                  scoring='neg_root_mean_squared_error', n_jobs=-1)

      run_time = timer() - start

      # Extract the best score
      loss = -cv_scores.mean() #negative to make the -RMSE positive so it can be minimized.
      #loss = cv_scores.mean()

      '''
      #best_score1 = np.min(cv_results['huber-mean'])
      #best_score2 = np.min(cv_results['quantile-mean'])
      best_score = np.min(cv_results['rmse-mean'])
      # Loss must be minimized
      #loss = 0.5 * best_score1 + 0.5 * best_score2 #1 - best_score
      loss = best_score
      # Boosting rounds that returned the best cv score
      #n_estimators = int(np.argmin(cv_results['huber-mean']) + 1)
      n_estimators = int(np.argmin(cv_results['rmse-mean']) + 1)
      print('best boost number: ',n_estimators,best_score)
      '''
      # Write to the csv file ('a' means append)
      of_connection = open(out_file, 'a')
      writer = csv.writer(of_connection)
      #writer.writerow([loss, params, ITERATION, n_estimators, run_time])
      writer.writerow([loss, params, ITERATION, run_time])

      # Dictionary with information for evaluation
      return {'loss': loss, 'params': params, 'iteration': ITERATION,
              #'estimators': n_estimators, 
              'train_time': run_time, 'status': STATUS_OK}

    # Run optimization

    best = fmin(fn = objective, space = space, algo = tpe_algorithm, 
                max_evals = max_trials, trials = bayes_trials, rstate = np.random.RandomState(50))

    # Save the trials database.
    pickle.dump(bayes_trials, open(outputdir+ "/gbm_trials.pkl", "wb"))

    # Sort the trials with lowest loss (highest AUC) first
    bayes_trials_results = sorted(bayes_trials.results, key = lambda x: x['loss'])

    self.params = bayes_trials_results[:1][0]['params']
    
# ------------------------------------------------------------------------------------------------
                
  # Algorithm behind the blocking strategy
  def __blockData(self,outputdir):

    if 'blocks' in globals():
      global blocks
    else:

      df=self.training_data.set_index(['lat', 'lon'])
      ds=df.PEATLAND_P.to_xarray().to_dataset()

      # Get total number of grid cells with data.
      ntotal=np.count_nonzero(~np.isnan(ds.to_array().values))
      print('Total number of grid cells with data: ',ntotal)
    
      # Get the desired number of grid cells per block.
      n_per_block=np.int(ntotal/nblocks)
      print('Target number of grid cells per block: ',n_per_block)
    
      # Initialize variables for loop.
      nsum=0 ; blocks=[]
      lon0=-180 ; lon=lon0 ; dlon=0.25 ; lon1=lon+dlon
      nsteps=np.int((180-lon0)/dlon)
  
      # Loop over each slice of dlon longitudes.
  
      for i in range(nsteps):
        # Accumulate the number of grid cells with data in each slice. 
        n=np.count_nonzero(~np.isnan(ds.sel(lon=slice(lon0,lon1)).to_array().values))
        nsum=nsum+n
        if nsum > n_per_block:
          # Store the bounds for the block.
          blocks.append([lon,lon1,-60,90,str(lon)+' to '+str(lon1)])
          lon=lon1
          nsum=0
        lon0=lon1
        lon1=lon1+dlon
      
      # Construct the final block.
      blocks.append([lon,lon0,-60,90,str(lon)+' to '+str(lon0)])
      
    # Output the blocks.
    np.savetxt(outputdir+'blocks.csv',blocks,delimiter=',',fmt="%s")
      
    # Get unique labels for each block/group.
      
    groups=self.training_data[["lat", "lon"]].copy()
    groups.insert(2,'label',0)
    label=1

    for block in blocks:
      mask = (self.training_data['lon'] > block[0]) & \
             (self.training_data['lon'] < block[1]) & \
             (self.training_data['lat'] > block[2]) & \
             (self.training_data['lat'] < block[3]) 
      groups.loc[mask,'label'] = label
      label=label+1

    self.groups=groups.label.values

# ------------------------------------------------------------------------------------------------

  # Makes plots, generates R^2, RMSE, etc.
  def generatePerformanceMetrics(self,outputdir):
          
    print("Generating Performance Metrics")
    
    # Each container in CVresults is a dict with results of various models
    for container in self.CVresults:
      print("\n" + container['area_name'])
      for model in self.models.keys():
        print(model)
        mse = metrics.mean_squared_error(container['actual_values'], container[model])
        r2 = metrics.r2_score(container['actual_values'], container[model])
        print("R2 = " + str(r2))
        print("RMSE = " + str(math.sqrt(mse)))
        plt.scatter(container['actual_values'], container[model], label=model, s=2)
      plt.xlabel("Real Values")
      plt.ylabel("Predicted Values")
      plt.title("Scatterplot of " + container['area_name'] + " cross-validation")
      plt.legend()
      plt.grid(b=True)
      #plt.savefig("output/plots/" + container['area_name'].replace(" ", "_") + ".pdf")
      plt.savefig(outputdir+"plots/" + container['area_name'].replace(" ", "_") + ".png")
      plt.clf()

# ------------------------------------------------------------------------------------------------

  # Retrain on ALL the data, then predict global peatland mapping
  def predictGlobalPeatlandFractions(self, model, outputdir):
    print("Performing global predictions...")
    X_train = self.training_data[self.features].values
    y_train = self.training_data['PEATLAND_P'].values.squeeze()

    self.models[model].fit(X_train,y_train)
    
    #print(self.score(X_train,y_train))
    # Prepare global prediction data
    X = self.predictors[self.features].values

    # Run the model globally and restrict values to between 0 and 100.
    pred_global = self.models[model].predict(X)
    pred_global = np.where(pred_global < 0, 0, pred_global)
    pred_global = np.where(pred_global > 100, 100, pred_global)
    self.pd_glob = pd.DataFrame(data=pred_global, dtype=float, columns=["PEATLAND_P"], index=self.predictors.index)

    # Reattach the lon/lat to the predicted values
    self.pd_glob = pd.concat([self.coordinates, self.pd_glob], axis=1)

 #   try:
    if model == 'lgb':
      # Output the selected features and their relative importance
      feature_importance=self.models[model].booster_.feature_importance(importance_type='gain')
      sorted_idx=np.argsort(feature_importance)
      importance_array=np.vstack([np.asanyarray(self.features)[sorted_idx], feature_importance[sorted_idx]]).T
      
      np.savetxt(outputdir+'feature_importance_gain.csv',importance_array,delimiter=',',fmt="%s")

      #feature_importance=self.models[model].feature_importances_
      feature_importance=self.models[model].booster_.feature_importance(importance_type='split')
      sorted_idx=np.argsort(feature_importance)
      importance_array=np.vstack([np.asanyarray(self.features)[sorted_idx], feature_importance[sorted_idx]]).T
      np.savetxt(outputdir+'feature_importance_split.csv',importance_array,delimiter=',',fmt="%s")

      fig = plt.figure(figsize=[16,10])
      ax = plt.subplot(1,1,1)
      lgb.plot_importance(self.models[model].booster_, ax=ax, height=0.2, 
              title='LightGBM Feature Importance', xlabel='Feature importance', ylabel='Features', 
              importance_type='gain', max_num_features=20, ignore_zero=True)

      plt.savefig(outputdir + model + "_importance.png")

      # Now do the metric over the training 
      #fig = plt.figure(figsize=[16,10])
      #ax = plt.subplot(1,1,1)
      #lgb.plot_metric(self.models[model].evals_result_, metric=None, ax=ax, xlim=None, ylim=None, title='Metric during training', xlabel='Iterations')
      #plt.savefig(outputdir + model + "_metric.png")

#    except:
#
# ------------------------------------------------------------------------------------------------
  # Use the same fractional file as a template for output
  def outputNetCDF(self, outputfile):
    df = self.pd_glob.set_index(['lat', 'lon'])
    self.fractions_xr['PEATLAND_P'] = df['PEATLAND_P'].to_xarray()
    self.fractions_xr.to_netcdf(outputfile)

# ------------------------------------------------------------------------------------------------
  def drawGlobalMap(self, model, outputdir):
    lons = self.fractions_xr['lon'].values
    lats = self.fractions_xr['lat'].values
    peatlands = self.fractions_xr['PEATLAND_P'].values
    fig = plt.figure(figsize=[16,10])
    ax = plt.subplot(1, 1, 1, projection=ccrs.PlateCarree())
    ax.set_extent([-180,180,-60,90])
    im = ax.pcolormesh(lons, lats, peatlands, transform=ccrs.PlateCarree())
    fig.colorbar(im, ax=ax)
    plt.savefig(outputdir + model + ".png")



if __name__ == "__main__":
  main()
